import express from 'express'
import bodyParser from 'body-parser'
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express'
import { execute, subscribe } from 'graphql'
import { createServer } from 'http'
import { SubscriptionServer } from 'subscriptions-transport-ws'
import cors from 'cors'
import schema from './schema'

const PORT = 4002

const app = express()

app.use('/graphql', bodyParser.json(), cors(), graphqlExpress(() => ({ schema })))
app.get('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
  subscriptionsEndpoint: `ws://localhost:${PORT}/subscriptions`,
}))

const ws = createServer(app)

ws.listen(PORT, () => {
  new SubscriptionServer({
    execute,
    subscribe,
    schema
  }, {
      server: ws,
      path: '/subscriptions',
    })
})

console.log('GraphQL Server is up')
