import { PubSub, withFilter } from 'graphql-subscriptions'

const pubsub = new PubSub()

const channels = {
  '1': {
    id: '1',
    messages: [],
  }
}

let nextMessageId = 2

export default {
  Query: {
    channel: (root, variables) => {
      return channels[variables.id]
    },
    channelIds: () => {
      return Object.keys(channels)
    }
  },
  Mutation: {
    addChannel: (root, variables) => {
      channels[variables.id] = { id: variables.id, messages: [] }
      pubsub.publish('channelAdded', { channelAdded: variables.id })
      return channels[variables.id]
    },
    addMessage: (root, variables) => {
      let newMessage = { id: nextMessageId++, text: variables.message.text }
      channels[variables.message.channelId].messages.push(newMessage)
      pubsub.publish('messageAdded', { messageAdded: newMessage, channelId: variables.message.channelId })
      return newMessage
    }
  },
  Subscription: {
    messageAdded: {
      subscribe: withFilter(
        () => pubsub.asyncIterator('messageAdded'),
        (payload, variables) => {
          console.log('MESSAGe ADDED')
          console.log(payload)
          return payload.channelId === variables.channelId;
        }
      )
    },
    channelAdded: {
      subscribe: () => pubsub.asyncIterator('channelAdded')
    }
  }
}




