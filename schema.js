import {
  makeExecutableSchema,
} from 'graphql-tools'
import resolvers from './resolvers'

const typeDefs = `
type Channel { 
  id: ID!
  messages: [Message]!
  createdAt: String!
  updatedAt: String!
}
type Message {
  id: ID!
  text: String!
  createdAt: String!
  updatedAt: String!
}
input MessageInput {
  channelId: ID!
  text: String!
}

type Query {
  channel(id: ID!): Channel
  channelIds: [ID]!
}
type Mutation {
  addChannel(id: ID!): Channel
  addMessage(message: MessageInput!): Message
} 
type Subscription {
  channelAdded: ID!
  messageAdded(channelId: ID!): Message
}
`

export default makeExecutableSchema({ typeDefs, resolvers })
